﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Locations;
using Android.Hardware;
using DropingDrops.Droid;
using DropingDrops;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DropingDrops.Droid;
using System.Threading.Tasks;
[assembly: Xamarin.Forms.Dependency(typeof(MainActivity))]
namespace DropingDrops.Droid
{
	[Activity(Label = "Main", MainLauncher = true)]


    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity, ILocationListener,ISensorEventListener, DropGeoCoordinates
    {
        ILocationListener locationListener;
        LocationManager locationManager;


        /** location updates should fire approximately every second */
        const int LOCATION_UPDATE_MIN_TIME_GPS = 1000;

        /** location updates should fire, even if last signal is same than current one (0m distance to last location is OK) */
        const int LOCATION_UPDATE_DISTANCE_GPS = 0;

        /** location updates should fire approximately every second */
        const int LOCATION_UPDATE_MIN_TIME_NW = 1000;

        /** location updates should fire, even if last signal is same than current one (0m distance to last location is OK) */
        const int LOCATION_UPDATE_DISTANCE_NW = 0;

        /** to faster access location, even use 10 minute old locations on start-up */
        const int LOCATION_OUTDATED_WHEN_OLDER_MS = 1000 * 60 * 10;

        /** is gpsProvider and networkProvider enabled in system settings */
        bool gpsProviderEnabled;
        bool networkProviderEnabled;
        
        public static Location lastKnownLocation;
        IWindowManager windowManager = Android.App.Application.Context.GetSystemService(Context.WindowService).JavaCast<IWindowManager>();
        ISensorEventListener sensorlistner;
        public static SensorManager mSensorManager ;
        public  static Sensor mGravity;
        public static  Sensor mAccelerometer;
        public static  Sensor mMagnetometer;
        public static bool haveGravity ;
        public static bool haveAccelerometer ;
        public static bool haveMagnetometer ;
        public static float mAzimuth ;
        public List<float[]> mRotHist = new List<float[]>();
        public int mRotHistIndex;
        public int mHistoryMaxLength = 20;
        public static float TWENTY_FIVE_DEGREE_IN_RADIAN = 0.436332313f;
        public static float ONE_FIFTY_FIVE_DEGREE_IN_RADIAN = 2.7052603f;
        float x;
        float y;
        public float[] gData1 = new float[3];
        public float[] gData = new float[3]; // gravity or accelerometer
        public float[] mData1 = new float[3];
        public float[] mData = new float[3]; // magnetometer
        public float[] rMat = new float[9];
        public float[] iMat = new float[9];
        public float[] orientation = new float[3];
        public static float inclination ;
        public static double[] loc;
        static List<double> l = new List<double>();
        
       


        protected override  void OnCreate(Bundle bundle)
		{     
			base.OnCreate(bundle);
            locationManager = LocationManager.FromContext(this);
            locationListener = this;
            
            if (this.locationManager != null && this.locationListener != null)
            {
                this.gpsProviderEnabled = this.locationManager.IsProviderEnabled(LocationManager.GpsProvider);
                this.networkProviderEnabled = this.locationManager.IsProviderEnabled(LocationManager.NetworkProvider);

                var currentTimeMillis = (DateTime.Now - new DateTime(1970, 1, 1)).TotalMilliseconds;

                if (gpsProviderEnabled)
                {
                    lastKnownLocation = locationManager.GetLastKnownLocation(LocationManager.GpsProvider);

                    if (lastKnownLocation != null && lastKnownLocation.Time > currentTimeMillis - LOCATION_OUTDATED_WHEN_OLDER_MS)
                        locationListener.OnLocationChanged(lastKnownLocation);

                    if (locationManager.GetProvider(LocationManager.GpsProvider) != null)
                        locationManager.RequestLocationUpdates(LocationManager.GpsProvider,
                           LOCATION_UPDATE_MIN_TIME_GPS, LOCATION_UPDATE_DISTANCE_GPS, this.locationListener);
                }

                if (networkProviderEnabled)
                {
                    lastKnownLocation = this.locationManager.GetLastKnownLocation(LocationManager.NetworkProvider);
                  
                    if (lastKnownLocation != null && lastKnownLocation.Time > currentTimeMillis - LOCATION_OUTDATED_WHEN_OLDER_MS)
                        locationListener.OnLocationChanged(lastKnownLocation);

                    
                }

                if (!gpsProviderEnabled && !networkProviderEnabled)
                    Toast.MakeText(this, "Please enable GPS and Network Positioning in your Settings", ToastLength.Short).Show();


            }

            mSensorManager = (Android.Hardware.SensorManager)ApplicationContext.GetSystemService(Context.SensorService);
            mGravity = mSensorManager.GetDefaultSensor(SensorType.Gravity);
            haveGravity = mSensorManager.RegisterListener(this, mGravity, SensorDelay.Fastest);
            mAccelerometer = mSensorManager.GetDefaultSensor(SensorType.Accelerometer);
            haveAccelerometer = mSensorManager.RegisterListener(this, mAccelerometer, SensorDelay.Fastest);

            mMagnetometer = mSensorManager.GetDefaultSensor(SensorType.MagneticField);
            haveMagnetometer = mSensorManager.RegisterListener(this, mMagnetometer, SensorDelay.Fastest);

            
            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());
           
		}
        
        static double[] Poicoordinates(double lat, double lon, double al, float mazimuth, float incli, double distance)
        {
            var inclin = distance * Math.Tan(incli - (Math.PI / 2));

            double EarthRadius = 6371000;
            var azimuth = mazimuth * Math.PI / 180;
            var b = distance / EarthRadius;
            var a = Math.Acos(Math.Cos(b) * Math.Cos((90 - lat) * Math.PI / 180) + Math.Sin((90 - lat) * Math.PI / 180) * Math.Sin(b) * Math.Cos(azimuth));
            var B = Math.Asin(Math.Sin(b) * Math.Sin(azimuth) / Math.Sin(a));

            var Lat2 = 90 - (a * 180 / Math.PI);
            var Lon2 = (B * 180 / Math.PI) + lon;
            var Al2 = al + inclin;
            return new double[] { Math.Round(Lat2, 5), Math.Round(Lon2, 5), Math.Round(Al2, 5) };


        }
   
      async  public void getdropgeocoordinates(double distance)
        {
            await Task.Delay(1600);
            
            loc = Poicoordinates(lastKnownLocation.Latitude, lastKnownLocation.Longitude, lastKnownLocation.Altitude, mAzimuth, inclination, distance);

            


            for (int i = 0; i < 3; i++)
            {
              

                l.Add(loc[i]);
            }
           

            

        }
        public List<double> getpoi()
        {
            return l;
        }
        #region ILocationListener implementation

        public void OnLocationChanged(Location location)
        {
           if (location != null)
                lastKnownLocation = location;



        }

        public void OnProviderDisabled(string provider)
        {
        }

        public void OnProviderEnabled(string provider)
        {
        }

        public void OnStatusChanged(string provider, Availability status, Bundle extras)
        {
        }
        #endregion
        #region ISensorEventListener implementation
        public void OnAccuracyChanged(Sensor sensor, [GeneratedEnum] SensorStatus accuracy)
        {

        }

        public void OnSensorChanged(SensorEvent e)
        {
            float[] data;
            float alpha = 0.97f;

            var ahna = e.Sensor.Type;
            switch (ahna)
            {
                case SensorType.Gravity:
                    for (int i = 0; i < 3; i++)
                    {
                        gData1[i] = e.Values[i];
                    }

                    break;
                case SensorType.Accelerometer:
                    for (int i = 0; i < 3; i++)
                    {
                        gData1[i] = e.Values[i];

                    }

                    break;
                case SensorType.MagneticField:
                    for (int i = 0; i < 3; i++)
                    {
                        mData1[i] = e.Values[i];
                    }


                    break;

                default: return;
            }
            var rotation = windowManager.DefaultDisplay.Rotation;

            if (SensorManager.GetRotationMatrix(rMat, null, gData1, mData1))
            {
                inclination = (float)Math.Acos(rMat[8]);


                if (inclination < TWENTY_FIVE_DEGREE_IN_RADIAN
                        || inclination > ONE_FIFTY_FIVE_DEGREE_IN_RADIAN)
                {

                    clearRotHist();

                    mAzimuth = float.NaN;
                }
                else
                {
                    setRotHist();


                    mAzimuth = (float)(findFacing() * 180 / Math.PI + 360) % 360;
                   


                    mSensorManager.UnregisterListener(this);


                }



            }
        }
        #endregion




 
      


        private void clearRotHist()
        {

            mRotHist.Clear();
            mRotHistIndex = 0;
        }

        private void setRotHist()
        {

            float[] hist = rMat;
            if (mRotHist.Count() == mHistoryMaxLength)
            {
                mRotHist.RemoveAt(mRotHistIndex);
            }
            mRotHist.Insert(mRotHistIndex++, hist);
            mRotHistIndex %= mHistoryMaxLength;
        }

        private float findFacing()
        {

            float[] averageRotHist = average(mRotHist);
            return (float)Math.Atan2(-averageRotHist[2], -averageRotHist[5]);
        }

        public float[] average(List<float[]> values)
        {
            float[] result = new float[9];
            foreach (float[] value in values)
            {
                for (int i = 0; i < 9; i++)
                {
                    result[i] += value[i];
                }
            }

            for (int i = 0; i < 9; i++)
            {
                result[i] = result[i] / values.Count();
            }

            return result;
        }
}
}


